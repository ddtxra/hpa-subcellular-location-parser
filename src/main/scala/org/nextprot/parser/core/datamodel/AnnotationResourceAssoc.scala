package org.nextprot.parser.core.datamodel

abstract class AnnotationResourceAssoc {

  val _resourceClass: String
  val _resourceType: String
  val _accession: String
  val _cvDatabaseName: String
  val _qualifierType: String
  val _isNegative: Boolean
  val _type: String

  def toXML =
    <com.genebio.nextprot.datamodel.annotation.AnnotationResourceAssoc>
      <resource class={ _resourceClass }>
        <resourceType>{ _resourceType }</resourceType>
        <accession>{ _accession }</accession>
        <cvDatabase>
          <cvName>{ _cvDatabaseName }</cvName>
        </cvDatabase>
      </resource>
      <qualifierType>{ _qualifierType }</qualifierType>
      <isNegativeEvidence>{ _isNegative }</isNegativeEvidence>
      <type>{ _type }</type>
    </com.genebio.nextprot.datamodel.annotation.AnnotationResourceAssoc>

}