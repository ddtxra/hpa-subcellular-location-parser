package org.nextprot.parser.hpa.subcell.rules

import org.nextprot.parser.core.constants.NXQuality._
import org.nextprot.parser.hpa.subcell.constants.HPAValidationValue._
import org.nextprot.parser.hpa.subcell.constants.HPAAPEReliabilityValue._

case class APEQualityRule(reliability: HPAAPEReliabilityValue, hpaPA: HPAValidationValue) {

  def getQuality: NXQuality =
    APEQualityRule.this match {
      case APEQualityRule(High, _) => GOLD
      case APEQualityRule(Medium, Supportive) => GOLD
      case APEQualityRule(Medium, Uncertain) => SILVER
      case APEQualityRule(Low, Supportive) => SILVER
      case APEQualityRule(Low, Uncertain) => BRONZE
      case APEQualityRule(Very_Low, _) => BRONZE
      case _ => throw new Exception("APEQualityRule not found: " + this)

    }

}