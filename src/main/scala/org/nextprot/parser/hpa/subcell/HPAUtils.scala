package org.nextprot.parser.hpa.subcell

import scala.xml.NodeSeq
import org.nextprot.parser.hpa.subcell.constants.HPAValidationValue.HPAValidationValue
import org.nextprot.parser.hpa.subcell.constants.HPAValidationValue

object HPAUtils {

  /**
   * Returns the 'enum' value for Western blot validation for a given antibody (Supportive, Uncertain, Not supportive)
   */
  def getWesternBlot(antibodyElem: NodeSeq): HPAValidationValue = {
    val HPAwbText = (antibodyElem \ "westernBlot" \ "verification").text;
    //In case there is no western blot experiment we use uncertain for western blot
    return HPAValidationValue.withName(if (HPAwbText.isEmpty()) "uncertain" else HPAwbText);
  }

  /**
   * Returns the 'enum' value for Protein array validation for a given antibody (Supportive, Uncertain, Not supportive)
   */
  def getProteinArray(antibodyElem: NodeSeq): HPAValidationValue = {
    val antibodyName = (antibodyElem \ "@id").text;
    //In case of CAB it is always supportive
    val HPApaText = if (antibodyName.startsWith("CAB")) "supportive" else (antibodyElem \ "proteinArray" \ "verification").text;
    return HPAValidationValue.withName(HPApaText);
  }
}