package org.nextprot.parser.hpa.subcell.constants

object HPAAPEReliabilityValue extends Enumeration {
  type HPAAPEReliabilityValue = Value
  val High, Low, Medium, Very_Low = Value

  final def withName(s: String)(implicit dummy: DummyImplicit): HPAAPEReliabilityValue = {

    s match {
      case "high" => return High;
      case "low" => return Low;
      case "medium" => return Medium;
      case "very low" => return Very_Low;
      case _ => throw new Exception(s + " not found for HPAAPEReliabilityValue")
    }

  }

}
