package org.nextprot.parser.hpa.subcell.cases
import org.nextprot.parser.core.datamodel._
import org.nextprot.parser.core.constants.NXQuality._
import org.nextprot.parser.hpa.subcell.constants.HPAAPEReliabilityValue._
import org.nextprot.parser.core.exception.NXExceptionType

//Cases to lok (isError = true)
/**
 * Error case where the reliabiliy is not available
 */
object RELIABILITY_NOT_AVAILABLE extends NXExceptionType(true, "The reliabiliy is not available");

object PROTEIN_NOT_DETECTED_BUT_LOCATION_EXISTENCE extends NXExceptionType(true, "The protein was not detected but contradictory information related to the location is shown");


/**
 * Error when the cell line was not tested the RNA
 */
object CELL_LINE_NOT_TESTED_FOR_RNA extends NXExceptionType(true, "The cell line was not tested the RNA");

/**
 * Error case where more than one antibody has been found for the experiment selected
 */
object CASE_MORE_THAN_ONE_ANTIBODY_FOUND_FOR_SELECTED extends NXExceptionType(true, "More than one antibody has been found for the experiment selected");
/**
 * Error case where the subcellular mapping for the location is not found in the current mapping file (does not exist in the mapping file)
 */
object CASE_SUBCELULLAR_MAPPING_NOT_FOUND extends NXExceptionType(true, "The subcellular mapping for this location is not found in the current mapping file (does not exist in the mapping file)");

//Cases to ignore (isError = false)
/**
 * Case where the quality is too low to be included in NextProt (this is not an error, but the entry must be discarded)
 */
object CASE_BRONZE_QUALITY extends NXExceptionType(false, "The quality is too low to be included in NextProt");

/**
 * Case where there is no subcellular location data available for the entry (this is not an error, but the entry must be discarded)
 */
object CASE_NO_SUBCELLULAR_LOCATION_DATA extends NXExceptionType(false, "There is no subcellular location data available for the entry");

/**
 * Case where there is no Uniprot / Swissprot mapping for the entry (this is not an error, but the entry must be discarded)
 */
object CASE_NO_UNIPROT_MAPPING extends NXExceptionType(false, "There is no Uniprot / Swissprot mapping for the entry");

/**
 * Case where the subcellular mapping for the given location is not applicable in the domain of NextProt (it appears a '-' in the mapping file) (this is not an error, but the entry must be discarded)
 */
object CASE_SUBCELULLAR_MAPPING_NOT_APPLICABLE extends NXExceptionType(false, "The subcellular mapping for the given location is not applicable in the domain of NextProt (it appears a '-' in the mapping file) ");

/**
 * Case where the RNA sequence was not detected in one or more cell lines (this is not an error, but the entry must be discarded)
 */
object CASE_RNA_NOT_DETECTED extends NXExceptionType(false, "The RNA sequence was not detected in one or more cell lines");
