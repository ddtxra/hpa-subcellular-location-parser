package org.nextprot.parser.hpa.subcell

import java.io.OutputStream
import org.nextprot.parser.core.NXParser
import java.io.File
import scala.xml.NodeSeq
import scala.collection.mutable.Map
import org.nextprot.parser.core.datamodel._
import org.nextprot.parser.hpa.subcell.rules.AntibodyValidationRule
import org.nextprot.parser.hpa.subcell.rules.AntibodyValidationRule
import org.nextprot.parser.hpa.subcell.constants.HPAValidationValue
import org.nextprot.parser.hpa.subcell.cases._
import org.nextprot.parser.core.constants.NXQuality._
import org.nextprot.parser.hpa.subcell.rules.APEQualityRule
import org.nextprot.parser.hpa.subcell.constants.HPAAPEReliabilityValue
import org.nextprot.parser.hpa.subcell.constants.HPAAPEReliabilityValue._
import org.nextprot.parser.core.exception.NXException
import org.nextprot.parser.core.exception.NXException
import org.nextprot.parser.core.exception.NXExceptionType

//Cases to lok (isError = true)

class HPANXParser extends NXParser {

  //Cases that should never happen but if they happen they should be logged
  val hpaSwissProtMapping = HPAConfig.getHpaSwissprotMapping; // The HPA-NX subcell location vocabulary map (Golgi apparatus -> SL-0132, Cytoplasm -> SL-0086, ...)
  def parseFile(file: File): AnnotationListWrapper = {

    val entryElem = scala.xml.XML.loadFile(file)

    checkPreconditions(file, entryElem)
    val quality = getQuality(file, entryElem);

    if (quality.equals(BRONZE))
      throw new NXException(CASE_BRONZE_QUALITY);

    val accession = (entryElem \ "identifier" \ "xref" \ "@id").text;

    new AnnotationListWrapper {

      val identifier = (entryElem \ "identifier" \ "@id").text;
      val _datasource = "Human protein atlas subcellular localization"
      val _accession = (entryElem \ "identifier" \ "xref" \ "@id").text
      val _rowAnnotations = List(extractSubcellularLocationInfoAnnotation(identifier, quality, entryElem)) ::: ((entryElem \ "subcellularLocation" \ "data" \ "location").map(extractSubcellularLocationAnnotation(identifier, quality, file, _))).toList
    }
  }

  private def checkPreconditions(file: File, elemNode: NodeSeq) = {

    val accession = (elemNode \ "identifier" \ "xref" \ "@id").text;

    if (accession.isEmpty())
      throw new NXException(CASE_NO_UNIPROT_MAPPING);

    if (!isValidForCellLines(file, elemNode)) {
      throw new NXException(CASE_RNA_NOT_DETECTED);
    }

    val locations = (elemNode \ "subcellularLocation" \ "data" \ "location").text;
    if (locations.isEmpty()) {
      throw new NXException(CASE_NO_SUBCELLULAR_LOCATION_DATA);
    } else {
      if ((elemNode \ "subcellularLocation" \ "summary").text == "The protein was not detected.")
        throw new NXException(PROTEIN_NOT_DETECTED_BUT_LOCATION_EXISTENCE)
    }

  }

  private def isValidForCellLines(file: File, EntryNode: NodeSeq): Boolean = {
    val rnamap = (EntryNode \ "rnaExpression" \ "data").map(f => ((f \ "cellLine").text, (f \ "level").text)).toMap;
    val cellLineList = (EntryNode \ "antibody" \ "subcellularLocation" \ "data" \ "cellLine").toList
    var isValid: Boolean = false

    if (cellLineList.isEmpty || rnamap.isEmpty) return true // No cell line data for antibodies or No RNAseq data
    cellLineList.foreach(cellLine => {
      //Check that the cell line exist in the RNA first
      if (!rnamap.contains(cellLine.text))
        throw new NXException(CELL_LINE_NOT_TESTED_FOR_RNA, (EntryNode \ "identifier" \ "xref" \ "@id").text + " has no RNA mapping for cell line " + cellLine.text)

      //At least one must be not "not detected" (at least one must be detected)
      isValid |= (rnamap(cellLine.text) != "Not detected")
    })
    return isValid
  }

  private def extractSubcellularLocationAnnotation(identifier: String, quality: NXQuality, file: File, locationElem: NodeSeq): RawAnnotation = {

    if (!hpaSwissProtMapping.contains(locationElem.text))
      throw new NXException(CASE_SUBCELULLAR_MAPPING_NOT_FOUND, locationElem.text)

    val location = hpaSwissProtMapping(locationElem.text);
    if (location.equals("-")) // Known cases of skipped mapping, eg: aggresome
      throw new NXException(CASE_SUBCELULLAR_MAPPING_NOT_APPLICABLE, locationElem.text)

    val status = (locationElem \ "@status").text;

    return new RawAnnotation {

      val _datasource = "Human protein atlas subcellular localization"
      val _isPropagableByDefault = true
      val _quality = quality
      val _cvTermAcc = hpaSwissProtMapping(locationElem.text)
      val _cvTermCategory = "Uniprot subcellular localization"
      val _description = "Found in " + locationElem.text + ". " + status.capitalize + " localization"
      val _type = "subcellular location"
      val _assocs = List(extractAnnotationResourceAssoc(identifier))
    }
  }

  private def extractSubcellularLocationInfoAnnotation(identifier: String, quality: NXQuality, entryElem: NodeSeq): RawAnnotation = {

    return new RawAnnotation {
      val _datasource = "Human protein atlas subcellular localization"
      val _isPropagableByDefault = true
      val _type = "subcellular location info"
      val _cvTermAcc = null
      val _cvTermCategory = null
      val _quality = quality
      val _description = (entryElem \ "subcellularLocation" \ "summary").text
      val _assocs = List(extractAnnotationResourceAssoc(identifier))
    }
  }

  private def extractAnnotationResourceAssoc(identifier: String): AnnotationResourceAssoc = {

    return new AnnotationResourceAssoc {
      val _resourceClass = "source.DbXref"
      val _resourceType = "DATABASE"
      val _accession = identifier
      val _cvDatabaseName = "HPA"
      val _qualifierType = "EXP"
      val _isNegative = false
      val _type = "EVIDENCE"
    }
  }

  /**
   * Returns the quality
   */
  private def getQuality(file: File, entryElem: NodeSeq): NXQuality = {
    val iftype = (entryElem \ "subcellularLocation" \ "@type").text.toLowerCase();
    val le = (entryElem \ "antibody" \ "subcellularLocation").length;

    iftype match {

      case "single" => {
        return getQualityForOneAntibody(entryElem \ "antibody")
      }
      case "selected" => {

        val selectedAntibody = (entryElem \ "antibody").filter(a => !(a \ "subcellularLocation").isEmpty)
        if (selectedAntibody.length != 1) {
          throw new NXException(CASE_MORE_THAN_ONE_ANTIBODY_FOUND_FOR_SELECTED, selectedAntibody.length + " antibodies ")
        }

        return getQualityForOneAntibody(selectedAntibody)

      }
      case "ape" => {
        return getQualityForIntegratedAntibody(entryElem)
      }
      case _ => throw new Exception(iftype + "not found")

    }

  }

  /**
   * Returns the quality for the single and selected case
   */
  private def getQualityForOneAntibody(antibodyElem: NodeSeq): NXQuality = {

    val HPAifText = (antibodyElem \ "subcellularLocation" \ "verification").text;
    
    val HPApa = HPAUtils.getProteinArray(antibodyElem);
    val HPAif = HPAValidationValue.withName(HPAifText);
    val HPAwb = HPAUtils.getWesternBlot(antibodyElem)

    new AntibodyValidationRule(HPApa, HPAif, HPAwb).getQuality;

  }

  /**
   * Returns the quality for the APE case
   */
  private def getQualityForIntegratedAntibody(entryElem: NodeSeq): NXQuality = {

    //Extract experiment reliability
    val reliabilityText = (entryElem \ "subcellularLocation" \ "verification").text;
    if (reliabilityText.equals("n/a")) {

      throw new NXException(RELIABILITY_NOT_AVAILABLE)
    }

    val reliability = HPAAPEReliabilityValue withName reliabilityText

    //It is always supportive if all antibodies are supportive (if the antibody is a CAB it is always supportive)
    var supportive = true;
    entryElem \ "antibody" foreach (a => supportive &= ((a \ "@id").text.startsWith("CAB") || ((a \ "proteinArray" \ "verification").text) == "supportive"))

    val HPApa = if (supportive) HPAValidationValue.Supportive else HPAValidationValue.Uncertain;

    return new APEQualityRule(reliability, HPApa).getQuality;

  }

}
