package org.nextprot.parser.hpa.subcell.rules

import org.nextprot.parser.core.constants.NXQuality._
import org.nextprot.parser.hpa.subcell.constants.HPAValidationValue._

case class AntibodyValidationRule(hpaPA: HPAValidationValue, hpaIF: HPAValidationValue, hpaWB: HPAValidationValue) {

  def getQuality: NXQuality =
    this match {
      case AntibodyValidationRule(Supportive, Supportive, Supportive) => GOLD
      case AntibodyValidationRule(Supportive, Supportive, Uncertain) => GOLD
      case AntibodyValidationRule(Supportive, Supportive, Not_Supportive) => SILVER
      case AntibodyValidationRule(Supportive, Uncertain, Supportive) => GOLD
      case AntibodyValidationRule(Supportive, Uncertain, Uncertain) => SILVER
      case AntibodyValidationRule(Supportive, Uncertain, Not_Supportive) => SILVER
      case AntibodyValidationRule(Supportive, Not_Supportive, Supportive) => BRONZE
      case AntibodyValidationRule(Supportive, Not_Supportive, Uncertain) => BRONZE
      case AntibodyValidationRule(Supportive, Not_Supportive, Not_Supportive) => BRONZE
      case AntibodyValidationRule(Uncertain, Supportive, Supportive) => SILVER
      case AntibodyValidationRule(Uncertain, Supportive, Uncertain) => SILVER
      case AntibodyValidationRule(Uncertain, Supportive, Not_Supportive) => SILVER
      case AntibodyValidationRule(Uncertain, Uncertain, Supportive) => SILVER
      case AntibodyValidationRule(Uncertain, Uncertain, Uncertain) => BRONZE
      case AntibodyValidationRule(Uncertain, Uncertain, Not_Supportive) => BRONZE
      case AntibodyValidationRule(Uncertain, Not_Supportive, Supportive) => BRONZE
      case AntibodyValidationRule(Uncertain, Not_Supportive, Uncertain) => BRONZE
      case AntibodyValidationRule(Uncertain, Not_Supportive, Not_Supportive) => BRONZE
      case _ => throw new Exception("AntibodyValidationRule not found: " + this)

    }

}