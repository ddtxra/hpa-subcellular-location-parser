package org.nextprot.parser.hpa

import org.scalatest._
import org.nextprot.parser.hpa.subcell.HPANXParser
import org.nextprot.parser.core.NXParser
import java.io.File
import org.nextprot.parser.core.exception.NXException
import org.nextprot.parser.hpa.subcell.cases.CASE_RNA_NOT_DETECTED
import org.nextprot.parser.hpa.subcell.HPAUtils
//import scala.xml.NodeSeq
import org.nextprot.parser.hpa.subcell.constants.HPAValidationValue._
import org.nextprot.parser.hpa.subcell.constants.HPAValidationValue

class ErrosCasesSpec extends FlatSpec with Matchers {
  
  System.setProperty("hpa.mapping.file", "src/test/resources/HPA_Subcell_Mapping.txt")

  "The HPANXParser parser" should " be an NXParser " in {
    val parser = new HPANXParser();
    assert(parser.isInstanceOf[NXParser])
  }

  it should "throw a NXException if the RNA Sequence is not found in the cell lines" in {
    val parser = new HPANXParser();
    a[NXException] should be thrownBy {
      parser.parseFile(new File("src/test/resources/ENSG_WITHOUT_RNA.xml"));
    }
  }

  it should "throw a NXException with NXExceptionType == CASE_RNA_NOT_DETECTED if the RNA Sequence is not found in the cell lines" in {
    val parser = new HPANXParser();
    val thrown = intercept[NXException] {
      parser.parseFile(new File("src/test/resources/ENSG_WITHOUT_RNA.xml"));
    }
    assert(thrown.getNXExceptionType == CASE_RNA_NOT_DETECTED)
  }

  // HPA_PARS_SPEC_C1
  "The Western blot validation parsing utility" should "return a 'Uncertain' value when there is no WB data for antibody" in {
    val antibodyElem = <antibody id="CAB004530" releaseDate="2006-10-30" releaseVersion="2.0"></antibody>;
    assert(HPAUtils.getWesternBlot(antibodyElem) == Uncertain)
  }
  // HPA_PARS_SPEC_C2
  "The Protein Array validation parsing utility" should "return a 'Supportive' value for CAB antibodies" in {
    val antibodyElem = <antibody id="CAB004530" releaseDate="2006-10-30" releaseVersion="2.0"></antibody>;
    assert(HPAUtils.getProteinArray(antibodyElem) == Supportive)
  }
}

